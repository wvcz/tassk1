package kz.aitu.project.entity;

import lombok.*;


@AllArgsConstructor
@Data
@ToString(includeFieldNames = false)
public class Student {
    private int studentId;
    private String studentName;
    private String phone;
    private int groupId;


}
