package kz.aitu.project.repository;

import kz.aitu.project.entity.Student;

import java.sql.*;

public class StudentRepository {
    String url = "jdbc:postgresql://localhost:5432/postgres";
    String user = "postgres";
    String password = "1234";
    Connection conn = null;
    Statement statement = null;

    public void showAllStudents() {
        try {
            conn = DriverManager.getConnection(url, user, password);
            statement = conn.createStatement();

            String sql = "SELECT * FROM student ORDER BY groupId";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                Student student = new Student(resultSet.getInt("StudentId") , resultSet.getString("StudentName") + " (name) ", resultSet.getString("phone"), resultSet.getInt("groupId"));
                System.out.println(student.toString());
            }
            resultSet.close();
        } catch (Exception se) {
            se.printStackTrace();
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException ignored) {

            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}






