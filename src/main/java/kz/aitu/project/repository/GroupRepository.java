package kz.aitu.project.repository;

import kz.aitu.project.entity.Group;

import java.sql.*;

    public class GroupRepository {
        String url = "jdbc:postgresql://localhost:5432/postgres";
        String user = "postgres";
        String password = "1234";
        Connection conn = null;
        Statement statement = null;

        public void showAllGroups() {
            try {
                conn = DriverManager.getConnection(url, user,password);
                statement = conn.createStatement();

                String sql = "SELECT * FROM groupq  ";
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    Group group = new Group(resultSet.getInt("groupId"), resultSet.getString("groupName"));
                    System.out.println(group.toString());
                }
                resultSet.close();
            } catch (Exception se) {
                se.printStackTrace();
            } finally {
                try{
                    if(statement!= null)
                        statement.close();
                    }catch (SQLException se) {
                    se.printStackTrace();
                }
                try {
                    if (conn != null)
                        conn.close();
                }catch (SQLException se){
                        se.printStackTrace();
                    }
                }
            }

        }









