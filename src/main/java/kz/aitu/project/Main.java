package kz.aitu.project;

import kz.aitu.project.repository.GroupRepository;
import kz.aitu.project.repository.StudentRepository;


public class Main {



    public static void main(String[] args) {


        System.out.println("Students list: ");
        StudentRepository studentRepository = new StudentRepository();
        studentRepository.showAllStudents();

        System.out.println("Groups list:");
        GroupRepository groupRepository = new GroupRepository();
        groupRepository.showAllGroups();



    }


}


